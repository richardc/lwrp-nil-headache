require 'spec_helper'

describe 'hello' do
  context 'absent' do
    let(:chef_run) do
      ChefSpec::SoloRunner.new(step_into: ['hello']).converge("test::absent")
    end

    it { expect(chef_run).to run_execute('greet absent').with_command(/no supplied message/) }
    it { expect(chef_run).to run_execute('no times absent') }
  end

  context 'empty' do
    let(:chef_run) do
      ChefSpec::SoloRunner.new(step_into: ['hello']).converge("test::empty")
    end

    it { expect(chef_run).to run_execute('greet empty').with_command(/message is ""/) }
    it { expect(chef_run).to run_execute('times empty') }
  end

  context 'value' do
    let(:chef_run) do
      ChefSpec::SoloRunner.new(step_into: ['hello']).converge("test::value")
    end

    it { expect(chef_run).to run_execute('greet value').with_command(/message is "value"/) }
    it { expect(chef_run).to run_execute('times value') }
  end

  context 'nil' do
    let(:chef_run) do
      ChefSpec::SoloRunner.new(step_into: ['hello']).converge("test::nil")
    end

    it { expect(chef_run).to run_execute('greet nil').with_command(/message is nil/) }
    it { expect(chef_run).to run_execute('times nil') }
  end
end
