resource_name :hello
property :name, String, name_property: true
property :message, String
property :times, Array, default: []

action :create do
  if property_is_set?(:message)
    execute "greet #{new_resource.name}" do
      command "echo 'Hello #{new_resource.name} message is #{new_resource.message.inspect}'"
    end
  else
    execute "greet #{new_resource.name}" do
      command "echo 'Hello #{new_resource.name} no supplied message'"
    end
  end

  if property_is_set?(:times)
    execute "times #{new_resource.name}" do
    end
  else
    execute "no times #{new_resource.name}" do
    end
  end
end
